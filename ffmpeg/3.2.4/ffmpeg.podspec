#
# Be sure to run `pod lib lint ffmpeg.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ffmpeg'
  s.version          = '3.2.4'
  s.summary          = 'A short description of ffmpeg.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/vyulabs'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'adarovsky' => 'adarovsky@vyulabs.com' }
  s.source           = { :git => 'https://bitbucket.org/vyulabs/ffmpeg.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.osx.deployment_target = '10.12'

  s.source_files = 'ffmpeg-libs/include/**/*.h'
  s.public_header_files = 'ffmpeg-libs/include/**/*.h'
  s.preserve_paths = 'ffmpeg-libs/include/**/*.h'
  s.header_mappings_dir = 'ffmpeg-libs/include'
  s.vendored_libraries = 'ffmpeg-libs/lib/*.a'
  s.frameworks = 'CoreGraphics', 'Security', 'CoreMedia', 'AVFoundation', 'CoreVideo', 'OpenGL', 'AppKit', 'QuartzCore', 'VideoToolbox', 'AudioToolbox', 'VideoDecodeAcceleration', 'CoreServices'
  s.osx.libraries = 'z', 'iconv'
end
