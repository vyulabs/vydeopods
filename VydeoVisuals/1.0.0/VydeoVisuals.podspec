#
# Be sure to run `pod lib lint VydeoVisuals.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'VydeoVisuals'
  s.version          = '1.0.0'
  s.summary          = 'A collection of simple custom views for Vydeo project'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
A collection of buttons with labels constrained to left and bottom,
a button with an icon located at the very left,
a custom stack view which lays out two items at the bottom and one by raw after that
                       DESC

  s.homepage         = 'https://fsme.sf.net'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'Proprietary', :file => 'LICENSE' }
  s.author           = { 'adarovsky' => 'adarovsky@vyulabs.com' }
  s.source           = { :git => 'https://bitbucket.org/vyulabs/vydeovisuals.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'VydeoVisuals/Classes/**/*'

  # s.resource_bundles = {
  #   'VydeoVisuals' => ['VydeoVisuals/Assets/*.png']
  # }

  s.public_header_files = 'VydeoVisuals/Classes/**/VV*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'KeepLayout'
end
