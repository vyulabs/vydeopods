#
# Be sure to run `pod lib lint reactivext.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'reactivext'
  s.version          = '1.1.4'
  s.summary          = 'A collection for Reactive ObjC extensions used in Vydeo'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
A simple collection of handy ReactiveCocoa (now ReactiveObjc) extensions used in VydeoSDK
                       DESC

  s.homepage         = 'https://github.com/adarovsky/reactivext'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'adarovsky' => 'adarovsky@vyulabs.com' }
  s.source           = { :git => 'ssh://git@bitbucket.org/vyulabs/reactivext.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'reactivext/Classes/**/*'

  s.public_header_files = 'reactivext/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'

  s.dependency 'AFNetworking', '~> 3.0'
  s.dependency 'ReactiveObjC'
  s.dependency 'VydeoVisuals', '>= 1.2.0'
  s.dependency 'CocoaLumberjack'
end
